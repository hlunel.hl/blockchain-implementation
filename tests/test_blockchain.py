from unittest.mock import MagicMock
from urllib.parse import ParseResult

import pytest
from requests import Response

from blockchain_implementation.Blockchain import Blockchain
from blockchain_implementation.model.Block import Block
from blockchain_implementation.model.Node import Node
from blockchain_implementation.model.Transaction import Transaction


@pytest.mark.freeze_time("2022-01-14")
def test_blockchain_initialization():
    # when
    blockchain = Blockchain()

    # then
    assert blockchain.current_transactions == []
    assert blockchain.chain == [
        Block(
            index=1,
            previous_hash="1",
            proof=100,
            timestamp=1642118400.0,
            transactions=[],
        )
    ]


def test_create_transaction():
    # given
    blockchain = Blockchain()

    # when
    blockchain.create_transaction(
        sender="sender",
        recipient="recipient",
        amount=5,
    )

    # then
    assert blockchain.current_transactions == [
        Transaction(
            sender="sender",
            recipient="recipient",
            amount=5,
        )
    ]


@pytest.mark.freeze_time("2022-01-14")
def test_create_block():
    # given
    blockchain = Blockchain()
    blockchain.create_transaction(
        sender="sender",
        recipient="recipient",
        amount=5,
    )

    # when
    blockchain.create_block(previous_hash="1", proof=100)

    # then
    assert blockchain.current_transactions == []
    assert blockchain.last_block == Block(
        index=2,
        previous_hash="1",
        proof=100,
        timestamp=1642118400.0,
        transactions=[
            Transaction(
                sender="sender",
                recipient="recipient",
                amount=5,
            )
        ],
    )


def test_add_node():
    # given
    blockchain = Blockchain()

    # when
    blockchain.add_node("http://127.0.0.1:8000")
    blockchain.add_node("http://127.0.0.1:8000")
    blockchain.add_node("http://127.0.0.1:8001")
    blockchain.add_node("http://127.0.0.1:8001")

    # then
    assert blockchain.nodes == {
        Node(
            address=ParseResult(
                scheme="http",
                netloc="127.0.0.1:8000",
                path="",
                params="",
                query="",
                fragment="",
            )
        ),
        Node(
            address=ParseResult(
                scheme="http",
                netloc="127.0.0.1:8001",
                path="",
                params="",
                query="",
                fragment="",
            )
        ),
    }


def test_is_valid_proof():
    # when & then
    assert Blockchain.is_valid_proof(current_proof=96659, last_proof=123456)
    assert not Blockchain.is_valid_proof(current_proof=654321, last_proof=123456)


def test_is_valid_chain():
    # given
    chain = [
        Block(
            index=1,
            timestamp=1642363944.4000738,
            transactions=[],
            proof=100,
            previous_hash="1",
        ),
        Block(
            index=2,
            timestamp=1642363963.3364627,
            transactions=[
                Transaction(
                    sender="0",
                    recipient="5cbd8a8062974ff4809de7b6d52a67af",
                    amount=1,
                )
            ],
            proof=35293,
            previous_hash="4ee5c1ad03da21e48ab8241c17240dce9e05190ca9e9e09f0576dd2533eb02a3",
        ),
        Block(
            index=3,
            timestamp=1642363964.036792,
            transactions=[
                Transaction(
                    sender="0",
                    recipient="5cbd8a8062974ff4809de7b6d52a67af",
                    amount=1,
                )
            ],
            proof=35089,
            previous_hash="9eb4f2a872af2b7f4701cabe3c066ab33d5f01dcbd9c8c1fe519479031b8b485",
        ),
        Block(
            index=4,
            timestamp=1642363964.9069633,
            transactions=[
                Transaction(
                    sender="0",
                    recipient="5cbd8a8062974ff4809de7b6d52a67af",
                    amount=1,
                )
            ],
            proof=119678,
            previous_hash="7f4399f93f15be67c189ece8121278ad5250071e18426855b3f5c051a9f55229",
        ),
    ]

    # when & then
    assert Blockchain.is_valid_chain(chain)


def test_is_valid_chain_with_wrong_previous_hash():
    # given
    chain = [
        Block(
            index=1,
            timestamp=1642363944.4000738,
            transactions=[],
            proof=100,
            previous_hash="1",
        ),
        Block(
            index=2,
            timestamp=1642363963.3364627,
            transactions=[
                Transaction(
                    sender="0",
                    recipient="5cbd8a8062974ff4809de7b6d52a67af",
                    amount=1,
                )
            ],
            proof=35293,
            previous_hash="4ee5c1ad03da21e48ab8241c17240dce9e05190ca9e9e09f0576dd2533eb02a3",
        ),
        Block(
            index=3,
            timestamp=1642363964.036792,
            transactions=[
                Transaction(
                    sender="0",
                    recipient="5cbd8a8062974ff4809de7b6d52a67af",
                    amount=1,
                )
            ],
            proof=35089,
            previous_hash="gnuiregge5ze95g16ernzangmvg1er65b1zr98",  # WRONG
        ),
    ]

    # when & then
    assert not Blockchain.is_valid_chain(chain)


def test_is_valid_chain_with_wrong_proof():
    # given
    chain = [
        Block(
            index=1,
            timestamp=1642363944.4000738,
            transactions=[],
            proof=100,
            previous_hash="1",
        ),
        Block(
            index=2,
            timestamp=1642363963.3364627,
            transactions=[
                Transaction(
                    sender="0",
                    recipient="5cbd8a8062974ff4809de7b6d52a67af",
                    amount=1,
                )
            ],
            proof=35293,
            previous_hash="4ee5c1ad03da21e48ab8241c17240dce9e05190ca9e9e09f0576dd2533eb02a3",
        ),
        Block(
            index=3,
            timestamp=1642363964.036792,
            transactions=[
                Transaction(
                    sender="0",
                    recipient="5cbd8a8062974ff4809de7b6d52a67af",
                    amount=1,
                )
            ],
            proof=999999,  # WRONG
            previous_hash="9eb4f2a872af2b7f4701cabe3c066ab33d5f01dcbd9c8c1fe519479031b8b485",
        ),
    ]

    # when & then
    assert not Blockchain.is_valid_chain(chain)


def test_resolve_conflicts_should_change_chain(mocker: MagicMock):
    # given
    blockchain = Blockchain()
    blockchain.add_node("http://127.0.0.1:8000")
    chain = [
        Block(
            index=1,
            timestamp=1642363944.4000738,
            transactions=[],
            proof=100,
            previous_hash="1",
        ),
        Block(
            index=2,
            timestamp=1642363963.3364627,
            transactions=[
                Transaction(
                    sender="0",
                    recipient="5cbd8a8062974ff4809de7b6d52a67af",
                    amount=1,
                )
            ],
            proof=35293,
            previous_hash="4ee5c1ad03da21e48ab8241c17240dce9e05190ca9e9e09f0576dd2533eb02a3",
        ),
    ]
    response = Response()
    response.status_code = 200
    response.json = MagicMock(name="json", return_value=chain)
    mocker.patch(
        "blockchain_implementation.Blockchain.requests.get",
        return_value=response,
    )

    # when
    blockchain.resolve_conflicts()

    # then
    assert blockchain.chain == chain


def test_resolve_conflicts_should_not_change_chain(mocker: MagicMock):
    # given
    blockchain = Blockchain()
    blockchain.add_node("http://127.0.0.1:8000")
    chain = [
        Block(
            index=1,
            timestamp=1642363944.4000738,
            transactions=[],
            proof=100,
            previous_hash="1",
        ),
    ]
    response = Response()
    response.status_code = 200
    response.json = MagicMock(name="json", return_value=chain)
    mocker.patch(
        "blockchain_implementation.Blockchain.requests.get",
        return_value=response,
    )

    # when
    blockchain.resolve_conflicts()

    # then
    assert not blockchain.chain == chain


def test_compute_hash():
    # given
    block = Block(
        previous_hash="9f68cc9948572979a6bc42e429152a7a5bfa2e11393f6dc7163f20dcc8359d40",
        timestamp=1642171673.9668658,
        proof=123456,
        transactions=[],
        index=5,
    )

    # when
    new_hash = Blockchain.compute_hash(block)

    # then
    assert (
        new_hash == "6cd618b61ac6d8795d67d8bd71c757b5793ff24b7184dc82ae43f895b819ee10"
    )


def test_compute_proof():
    # when & then
    assert Blockchain.compute_proof(last_proof=123456) == 96659
