#!/bin/sh

PROJECT_NAME="blockchain"
IMAGE_NAME="$PROJECT_NAME-image"
PORT=$1
CONTAINER_NAME="$PROJECT_NAME-container-$PORT"

echo "Starting new container $CONTAINER_NAME ..."
sudo docker run --rm --name $CONTAINER_NAME -p $PORT:5000 $IMAGE_NAME:latest

