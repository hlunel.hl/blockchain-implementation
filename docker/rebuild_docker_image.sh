#!/bin/sh

PROJECT_NAME="blockchain"
IMAGE_NAME="$PROJECT_NAME-image"

echo "Building new image $IMAGE_NAME ..."
sudo docker build --cache-from $IMAGE_NAME:latest -t $IMAGE_NAME:latest -f Dockerfile ..

