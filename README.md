# Blockchain implementation

## Summary

This project is a training project to learn about blockchains.

## Usage

1) Initialize Docker image

```shell
> cd docker
> sudo sh ./rebuild_docker_image.sh
```

2) Start Docker container

```shell
> sudo docker/start_docker_container.sh <port>
```

You can use this command multiple times with different ports to simulate a blockchain with multiple nodes.

P.S: You can interact with the node at http://localhost:5000/docs (change the port).

## Resource used

https://hackernoon.com/learn-blockchains-by-building-one-117428612f46
