from pydantic import BaseModel

from blockchain_implementation.model.Transaction import Transaction


class Block(BaseModel):
    index: int
    timestamp: float
    transactions: list[Transaction]
    proof: int
    previous_hash: str
