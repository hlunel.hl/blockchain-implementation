from urllib.parse import ParseResult

from pydantic import BaseModel


class Node(BaseModel):
    address: ParseResult

    def __hash__(self) -> int:
        return hash(str(self))
