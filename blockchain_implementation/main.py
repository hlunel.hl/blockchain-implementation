from uuid import uuid4

import uvicorn
from fastapi import FastAPI
from loguru import logger
from fastapi.middleware.cors import CORSMiddleware

from blockchain_implementation.Blockchain import Blockchain
from blockchain_implementation.model.Block import Block
from blockchain_implementation.model.Node import Node
from blockchain_implementation.model.Transaction import Transaction

NODE_IDENTIFIER = str(uuid4()).replace("-", "")
BLOCKCHAIN = Blockchain()
BLOCK_REWARD_AMOUNT = 1

app = FastAPI()

ALLOW_CREDENTIALS = False
ALLOWED_ORIGINS = ["*"]
ALLOWED_METHODS = ["*"]
ALLOWED_HEADERS = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_credentials=ALLOW_CREDENTIALS,
    allow_origins=ALLOWED_ORIGINS,
    allow_methods=ALLOWED_METHODS,
    allow_headers=ALLOWED_HEADERS,
)


@app.post(path="/mine", response_model=Block)
async def mine():
    logger.info("Received request to mine next block")
    last_block = BLOCKCHAIN.last_block
    new_block_proof = BLOCKCHAIN.compute_proof(last_block.proof)

    BLOCKCHAIN.create_transaction(
        sender="0",
        recipient=NODE_IDENTIFIER,
        amount=BLOCK_REWARD_AMOUNT,
    )

    previous_hash = BLOCKCHAIN.compute_hash(last_block)
    return BLOCKCHAIN.create_block(previous_hash, new_block_proof)


@app.post(path="/transactions", response_model=Transaction)
async def create_transaction(sender: str, recipient: str, amount: int):
    logger.info(
        "Received request to create transaction with values: sender={}, recipient={}, amount={}",
        sender,
        recipient,
        amount,
    )
    return BLOCKCHAIN.create_transaction(sender, recipient, amount)


@app.get(path="/chain", response_model=list[Block])
async def get_chain():
    logger.info("Received request to get chain")
    return BLOCKCHAIN.chain


@app.post(path="/register-node", response_model=Node)
async def register_node(address: str):
    logger.info("Received request to add node with address={}", address)
    return BLOCKCHAIN.add_node(address)


@app.post(path="/resolve-conflicts")
async def resolve_conflicts():
    logger.info("Received request to resolve conflicts")
    BLOCKCHAIN.resolve_conflicts()
    return "OK"


def main():
    uvicorn.run(
        "blockchain_implementation.main:app",
        host="0.0.0.0",
        port=5000,
        reload=True,
        debug=True,
        workers=1,
    )


if __name__ == "__main__":
    main()
