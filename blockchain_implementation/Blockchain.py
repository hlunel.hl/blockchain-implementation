import hashlib
import json
from time import time
from urllib.parse import urlparse

import requests
from loguru import logger

from blockchain_implementation.model.Block import Block
from blockchain_implementation.model.Node import Node
from blockchain_implementation.model.Transaction import Transaction

NUMBER_OF_LEADING_ZEROS = 4


class Blockchain(object):
    def __init__(self):
        self.chain: list[Block] = []
        self.current_transactions: list[Transaction] = []
        self.nodes: set[Node] = set()

        self.create_block(previous_hash="1", proof=100)

    def create_block(
        self,
        previous_hash: str,
        proof: int,
    ) -> Block:
        new_block = Block(
            index=len(self.chain) + 1,
            previous_hash=previous_hash,
            proof=proof,
            timestamp=time(),
            transactions=self.current_transactions,
        )
        self.chain.append(new_block)
        self.current_transactions = []
        return new_block

    def create_transaction(
        self,
        sender: str,
        recipient: str,
        amount: int,
    ) -> Transaction:
        new_transaction = Transaction(sender=sender, recipient=recipient, amount=amount)
        self.current_transactions.append(new_transaction)
        return new_transaction

    def add_node(self, address: str) -> Node:
        node = Node(address=urlparse(address))
        self.nodes.add(node)
        return node

    def resolve_conflicts(self) -> None:
        max_length = len(self.chain)
        for node in self.nodes:
            base_url = node.address.geturl()
            url = f"{base_url}/chain"
            logger.info("Sending request to get chain to node with url: {}", url)
            response = requests.get(url)
            if response.status_code == 200:
                chain_json = response.json()
                chain = [Block(**block_json) for block_json in chain_json]
                chain_length = len(chain)
                if chain_length > max_length and Blockchain.is_valid_chain(chain):
                    max_length = chain_length
                    self.chain = chain
            else:
                logger.error(
                    "Error while sending request to node with url {}. Status:{}",
                    url,
                    response.status_code,
                )

    @property
    def last_block(self) -> Block:
        return self.chain[-1]

    @staticmethod
    def compute_hash(block: Block) -> str:
        block_string = json.dumps(
            block,
            default=lambda o: o.__dict__,
            sort_keys=True,
        ).encode()
        return hashlib.sha256(block_string).hexdigest()

    @staticmethod
    def compute_proof(last_proof: int) -> int:
        current_proof = 0
        while True:
            is_valid_proof = Blockchain.is_valid_proof(current_proof, last_proof)
            if is_valid_proof:
                return current_proof
            current_proof += 1

    @staticmethod
    def is_valid_proof(current_proof: int, last_proof: int):
        current_guess = f"{last_proof}{current_proof}".encode()
        current_guess_hash = hashlib.sha256(current_guess).hexdigest()
        return (
            current_guess_hash[:NUMBER_OF_LEADING_ZEROS]
            == "0" * NUMBER_OF_LEADING_ZEROS
        )

    @staticmethod
    def is_valid_chain(chain: list[Block]) -> bool:
        previous_block = chain[0]
        for current_block in chain[1:]:
            is_valid_hash = (
                Blockchain.compute_hash(previous_block) == current_block.previous_hash
            )
            if not is_valid_hash:
                return False

            is_valid_proof = Blockchain.is_valid_proof(
                current_proof=current_block.proof, last_proof=previous_block.proof
            )
            if not is_valid_proof:
                return False

            previous_block = current_block
        return True
